# AHE FLASK
from flask import Flask

app = Flask(__name__)
app.secret_key = "toto"


@app.route('/',  methods=['GET'])
def home():
    return "Hello World"
